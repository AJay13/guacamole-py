#!/usr/bin/env python
# -*- coding:utf-8 -*- 
# vim: set et sw=4 ts=4 sts=4 ff=unix fenc=utf8:

"""
@version: 1.0
@author: readerror
@contact: readerror@readerror.cn
@contact1: readerror000@gmail.com
@site: http://www.readerror.cn
@software: PyCharm
@file: example.py
@time: 2017/2/15 19:09
"""

import guacamole

if __name__ == '__main__':
    ADMIN_USERNAME = 'guacadmin'
    ADMIN_PASSWORD = 'guacadmin'
    GUACAMOLE_HOST = '192.168.18.144'
    GUACAMOLE_PORT = 8081
    GUACAMOLE_SERVER_PATH = 'guaca'

    username = 'admin'
    password = '1234qwerASDF'
    remote_ip = '192.168.18.166'
    remote_host_username = 'user'
    remote_host_password = '1'
    performance = 1  # 0-5 (桌面、主题、ClearType、Aero、菜单动画、声音、麦克风、打印机)
    connection_name = remote_ip

    client = guacamole.Client(ADMIN_USERNAME, ADMIN_PASSWORD, GUACAMOLE_HOST, GUACAMOLE_PORT, GUACAMOLE_SERVER_PATH)
    if client.login() is not None:
        # Delete user, group named username ,connections belong the group(Last connection coulden't delete auto).
        guacamole.delete_user_all_residuals(client, username)

        # Delete connection named connection_name (For connection created in 'ROOT' group).
        guacamole.delete_connections(client, connection_name)

        # Delete the connection belong the user.
        # client.delete_connection(client.get_user_connection_id(username, connection_name))

        rdp_url = guacamole.create_user_connection(client, username=username, password=password, remote_ip=remote_ip,
                                                   host_username=remote_host_username,
                                                   host_password=remote_host_password,
                                                   performance=performance, connection_name=connection_name,
                                                   connection_type='rdp', only_one=True)
        print(rdp_url)

    client.logout()

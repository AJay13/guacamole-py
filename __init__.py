#!/usr/bin/env python
# -*- coding:utf-8 -*- 
# vim: set et sw=4 ts=4 sts=4 ff=unix fenc=utf8:

"""
@version: 1.0
@author: readerror
@contact: readerror@readerror.cn
@contact1: readerror000@gmail.com
@site: http://www.readerror.cn
@software: PyCharm
@file: __init__.py
@time: 2017/2/15 8:53
"""

from .guacamole import Client, create_user_connection, delete_user_all_residuals, delete_connections

__version__ = version = "1.0.0"
version_info = tuple([int(d) for d in version.split("-")[0].split(".")])
__title__ = 'guacamole-py'